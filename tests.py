import unittest
from src.app import db, app
from src.core.contact import Contact

class TestContactModel(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQL_ALCHEMY_DATABASE_URI'] = 'sqlite:///db/ttpd_test.db'
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def create_contact_helper(self):
        payload = {
            "username": "TestUser",
            "emails":["test1@test.com", "test2@test.com"],
            "first_name": "TestName",
            "surname":"TestSurname"
        }

        Contact().create(payload)

    def test_create_contact(self):
        record_exists = db.session.query(Contact).filter(Contact.username == "TestUser").first()
        self.assertEqual(record_exists, None)

        self.create_contact_helper()

        record_created = db.session.query(Contact).filter(Contact.username == "TestUser").count()
        self.assertEqual(record_created, 1)

    def test_create_contact_failed_validations(self):
        print("TODO")

    def test_get_all_contacts(self):
        total_records_pre = db.session.query(Contact).count()
        self.assertEqual(total_records_pre, 0)

        self.create_contact_helper()
        self.create_contact_helper()

        query_params = {}
        result = Contact().get_one_or_all(query_params)

        total_records_post = db.session.query(Contact).count()
        self.assertEqual(total_records_post, 2)

    def test_get_contacts_with_email_query_param(self):
        self.create_contact_helper()
        self.create_contact_helper()

        query_params = {"email": "test1@test.com"}
        result = Contact().get_one_or_all(query_params)
        self.assertEqual(len(result), 2)

    def test_get_contacts_with_any_whitelisted_query_param(self):
        self.create_contact_helper()
        self.create_contact_helper()

        existing_record = db.session.query(Contact).first()

        query_params = {"id": existing_record.id}
        result = Contact().get_one_or_all(query_params)
        self.assertEqual(existing_record.id, result["id"])

    def test_get_contacts_with_any_non_whitelisted_query_param(self):
        print("TODO")         

    def test_update_contact(self):
        print("TODO")

    def test_update_missing_contact(self):
        print("TODO")   

    def test_delete_contact(self):
        print("TODO")  

    def test_delete_missing_contact(self):
        print("TODO")                    

if __name__ == '__main__':
    unittest.main()
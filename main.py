from src.app import app
from src.routes.main import *
from src.models import *

if __name__ == "__main__":
    app.run(host='0.0.0.0:5000')

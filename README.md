## SETUP

- Use virtualenv with python 3.6.6. 
- Install dependencies with pip.

## RUN

- `FLASK_APP=main.py`
- `flask run`

## RUN TESTS

`python tests.py`

## RUN CELERY

- `celery -A tasks_app worker --app=celery_tasks:celery_app -B --loglevel=debug
from src.app import app
from src.app import db
from src.models import *
import json
from flask import Response


def create_contact(request):
    payload = request.json

    contact = Contact()
    result = contact.create(payload)

    return result


def get_one_or_all_contacts(request):
    query_params = request.args

    contact = Contact()
    result = contact.get_one_or_all(query_params)

    return result


def update_contact(request):
    payload = request.json

    contact = Contact()
    result = contact.update(payload)

    return result


def delete_contact(request):
    query_params = request.args

    contact = Contact()
    result = contact.delete(query_params)
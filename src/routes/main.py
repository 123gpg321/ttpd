from src.app import app
from src.routes.contact import *
from flask import request


@app.route("/contact", methods=['GET', 'POST', 'PUT', 'DELETE'])
def contact_handler():
    if request.method == 'GET':
        result = get_one_or_all_contacts(request)
        return Response(json.dumps(result), status=200, mimetype='application/json')
    elif request.method == 'POST':
        result = create_contact(request)
        return Response(json.dumps(result), status=201, mimetype='application/json')
    elif request.method == 'PUT':
        result = update_contact(request)
        return Response(json.dumps(result), status=200, mimetype='application/json')
    elif request.method == 'DELETE':
        delete_contact(request)
        return Response('{}', status=200, mimetype='application/json')
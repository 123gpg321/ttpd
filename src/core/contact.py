from src.app import db
from src.core.email import Email
import datetime


class Contact(db.Model):
    __tablename__ = 'contacts'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128))
    first_name = db.Column(db.String(128))
    surname = db.Column(db.String(128))
    emails = db.relationship('Email', backref='contact', lazy='dynamic')
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def serialize(self):
        return {
            'id': self.id, 
            'username': self.username,
            'first_name': self.first_name,
            'surname': self.surname,
            'emails': [x.email for x in self.emails.all()]
        }

    def create(self, payload):
        emails = [Email(email=x) for x in payload["emails"]]
        contact_with_emails = Contact(
            username=payload['username'],
            first_name=payload['first_name'],
            surname=payload['surname'],
            emails=emails
        )
        db.session.add(contact_with_emails)
        db.session.commit()

        return contact_with_emails.serialize()

    def get_one_or_all(self, query_params):
        if len(query_params) == 0:
            result = db.session.query(Contact).all()
            serialized_result = [record.serialize() for record in result]
        elif len(query_params) == 1:
            param_name = list(query_params.keys())[0]
            if param_name == 'email':
                result = db.session.query(Contact).filter(
                    Contact.emails.any(email=query_params['email'])).all()
                serialized_result = [record.serialize() for record in result]
            else:
                result = db.session.query(Contact).filter(
                    getattr(Contact, param_name) == query_params[param_name]).first()
                serialized_result = result.serialize()

        return serialized_result

    def update(self, payload):
        result = db.session.query(Contact).filter(
            Contact.id == int(payload['id'])).first()

        result.username = payload['username']
        result.first_name = payload['first_name']
        result.surname = payload['surname']
        db.session.commit()

        return result.serialize()
    
    def delete(self, query_params):
        result = db.session.query(Contact).filter(
            Contact.id == int(query_params['id'])).delete(synchronize_session='fetch')
        db.session.commit()
from src.app import db


class Email(db.Model):
    __tablename__ = 'emails'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128))
    contact_id = db.Column(db.Integer(), db.ForeignKey('contacts.id'))


    def __init__(self, email=None):
        self.email = email
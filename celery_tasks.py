from celery import Celery
from src.app import db, app
import datetime
from src.core.contact import Contact
from faker import Faker


celery_app = Celery('tasks_app', broker=app.config['CELERY_BROKER_URL'])
celery_app.conf.update(app.config)

@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
    	15.0,
    	create_random_contact.s(),
    	name='create_random_contact every 15'
    )

    sender.add_periodic_task(
    	20.0,
    	rolling_window_contacts_cleaner.s(),
    	name='rolling_window_contacts_cleaner every 20'
    )

@celery_app.task
def create_random_contact():
	f = Faker()
	payload = {
		"username": f.user_name(),
		"emails":[f.email(),
		 f.email()],
		 "first_name": f.first_name(),
		 "surname": f.first_name()
	 }
	Contact().create(payload)

@celery_app.task
def rolling_window_contacts_cleaner():
	one_min_ago = datetime.datetime.utcnow() - datetime.timedelta(minutes=1)
	db.session.query(Contact).filter(
		Contact.created_at < one_min_ago).delete(synchronize_session=False)